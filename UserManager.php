<?php

require_once "User.php";
class UserManager
{
    private $DB;
    public function __construct()
    {
        $DBname = "website";
        $username = "root";
        $password = "";
        $port = 3306;

        try {
            $this->setDB(new PDO("mysql:host=localhost;dbname=$DBname;port=$port;charset=utf8mb4", $username, $password));
        } catch (PDOException $error) {
            echo $error->getMessage();
        }

    }

    public function setDB($DB)
    {

        $this->DB = $DB;

    }

    public function create(User $newUser)
    {
        $request = $this->DB->prepare("INSERT INTO `users`(username, password, email, isAdmin, creationDate) VALUES(:username, :password, :email, :isAdmin, :creationDate)");
        $request->bindValue(":username", $newUser->getUsername(), PDO::PARAM_STR);
        $request->bindValue(":password", $newUser->getPassword(), PDO::PARAM_STR);
        $request->bindValue(":email", $newUser->getEmail(), PDO::PARAM_STR);
        $request->bindValue(":isAdmin", $newUser->getIsAdmin(), PDO::PARAM_BOOL);
        $request->bindValue(":creationDate", $newUser->getCreationDate(), PDO::PARAM_STR);

        $request->execute();
    }

    public function read($username)
    {
        $request = $this->DB->prepare("SELECT * FROM `users` WHERE username = :username");
        $request->bindValue(":username", $username, PDO::PARAM_STR);
        $request->execute();
        $result = $request->fetch();
        return new User($result);
    }

    public function readAll()
    {
        $request = $this->DB->prepare("SELECT * FROM `users`");
        $request->execute();
        $result = $request->fetchall();
        $userarray = [];
        foreach ($result as $element) {
            array_push($userarray, $element);
        }
        return $userarray;
    }

    public function delete($ID)
    {
        $request = $this->DB->prepare("DELETE * FROM `users` WHERE ID = :id");
        $request->bindValue(":id", $ID, PDO::PARAM_INT);
        $request->execute();
    }

}

?>