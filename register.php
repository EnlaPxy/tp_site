<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootleg</title>
    <link rel="stylesheet" href="bulma.css">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="navbar.js"></script>
</head>

<body class="has-navbar-fixed-top"
    style="background-image: url(assets/Death_Grips_at_Brooklyn_Masonic_Temple\,_NYC_\(2015\).jpg); height:100vh">
    <div id=navbar>
        <script src="navbar.js"></script>
    </div>

    <section class="section container">
        <div class="box">
            <form class="form" method="post">
                <div class="control">
                    <div class="field">
                        <label for="username" class="label">Username : </label>
                        <input class="input" id="username" type="text" placeholder="Username" name="username"
                            autocomplete="username">
                    </div>
                    <div class="field">
                        <label for="password" class="label">Password : </label>
                        <input class="input" id="password" type="password" placeholder="Password" name="password"
                            autocomplete="new-password">
                    </div>
                    <div class="field">
                        <label for="email" class="label">E-mail address : </label>
                        <input class="input" id="email" type="email" placeholder="Email" autocomplete="email"
                            name="email">
                    </div>
                    <div class="field">
                        <button class="button is-primary"><strong>Register</strong></button>
                    </div>
                </div>
            </form>
        </div>

    </section>

    <?php
    require_once "UserManager.php";
    require_once "User.php";
    date_default_timezone_set("Europe/Paris");

    if ($_POST) {
        $now = mktime(date("m"), date("d"), date("Y"));
        $now = date('Y-m-j', $now);

        $UserManager = new UserManager();
        $User = new User($_POST);
        $User->setDate($now);
        $UserManager->create($User);
    }

    ?>

</body>