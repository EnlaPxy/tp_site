<?php

session_start();

?>
<nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">

  <div class="navbar-brand">
    <a class="navbar-item" href="index.html">
      <img
        src="https://www.studsandspikes.com/media/catalog/product/cache/a38e7bbb3083ca5c0882db29241119eb/9/p/9pts22deat03_design.jpg">
    </a>

    <a role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>


  </div>

  <div id="navbarBasicExample" class="navbar-menu is-fixed-top">
    <div class="navbar-start">
      <a class="navbar-item" href="index.html">
        Home
      </a>

      <a class="navbar-item" href="blog.php">
        Blog
      </a>

      <div class="navbar-item has-dropdown is-hoverable is-fixed-top">
        <a class="navbar-link">
          More
        </a>

        <div class="navbar-dropdown">
          <a class="navbar-item">
            About
          </a>
          <hr class="navbar-divider">
          <a class="navbar-item" href="contact.php">
            Report an issue
          </a>
        </div>
      </div>
    </div>

    <?php
    //var_dump($_SESSION); // Debug Value
    
    if ($_SESSION == NULL) {   //code qui vérifie si l'utilisateur est connecté et qui affiche les boutons au besoin.
      ?>
      <div class="navbar-end">
        <div class="navbar-item">
          <div class="buttons">
            <a class="button is-primary" href="register.php">
              <strong>Sign up</strong>
            </a>
            <a class="button is-light" href="login.php">
              Log in
            </a>
          </div>
        </div>
      </div>
      <?php
    } else { //si l'utilisateur est connecté, lui donner la possibilité de créer un article et de se déconnecter. 
      ?>
      <div class="navbar-end\">
        <div class="buttons">
          <div class="navbar-item">
            <a class="button is-secondary" href="writeArticle.php">Write article</a>
          </div>
          <div class="navbar-item\">
            <a class="button is-primary" href="disconnect.php">
              <strong>Disconnect</strong>
            </a>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
</nav>