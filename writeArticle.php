<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootleg</title>
    <link rel="stylesheet" href="bulma.css">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="navbar.js"></script>
</head>

<body class="has-navbar-fixed-top"
    style="background-image: url(assets/Death_Grips_at_Brooklyn_Masonic_Temple\,_NYC_\(2015\).jpg); background-size: 100% 100%">
    <div id=navbar>
        <script src="navbar.js"></script>
    </div>
    <section class="section">
        <div class="box" style="width: 50%; margin: 25% ; margin-top: 0%">
            <form class="form" method="post">
                <h1>Write an Article :</h1>
                <br>
                <div>
                    <input class="input" type="text" placeholder="Title" name="title">
                    <br><br>
                    <textarea name="content" class="textarea" placeholder="Write your article..." rows="10"></textarea>
                    <br>
                    <input class="button is-primary" type="submit" name="button">
                </div>
            </form>
        </div>
    </section>
    <?php
    require_once "ArticleManager.php";
    require_once "Article.php";
    session_start();

    $ArtMan = new ArticleManager();
    $artarray = $ArtMan->readAll();
    var_dump($artarray);

    if ($_POST) {
        $now = mktime(date("m"), date("d"), date("Y"));
        $now = date('Y-m-j', $now);

        $ArticleManager = new ArticleManager();
        $Article = new Article($_POST);
        $Article->setDate($now);
        $Article->setAuthor($_SESSION["username"]);
        $ArticleManager->create($Article);
    }

    ?>

</body>