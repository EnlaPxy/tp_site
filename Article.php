<?php

class Article
{
    private $title;
    private $content;
    private $author;
    private $creationDate = 0;

    public function __construct(array $array)
    {
        $this->hydrate($array);
    }

    public function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $var = "set" . ucfirst($key);
            if (method_exists($this, $var)) {
                $this->$var($value);
            }
        }
    }

    public function setTitle($title)
    {

        $this->title = $title;

    }

    public function setContent($content)
    {

        $this->content = $content;

    }

    public function setAuthor($author)
    {

        $this->author = $author;

    }

    public function setDate($creationDate)
    {

        $this->creationDate = $creationDate;

    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

}