<?php

class User
{
    private $username;
    private $password;
    private $email;
    private $isAdmin = 0;
    private $creationDate = 0;

    public function __construct(array $array)
    {
        $this->hydrate($array);
    }

    public function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $var = "set" . ucfirst($key);
            if (method_exists($this, $var)) {
                $this->$var($value);
            }
        }
    }

    public function setUsername($username)
    {

        $this->username = $username;

    }

    public function setPassword($password)
    {

        $this->password = $password;

    }

    public function setEmail($email)
    {

        $this->email = $email;

    }

    public function setDate($creationDate)
    {

        $this->creationDate = $creationDate;

    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function getIsAdmin()
    {
        return $this->isAdmin;
    }


}