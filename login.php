<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootleg</title>
    <link rel="stylesheet" href="bulma.css">
    <script type="text/javascript" src="navbar.js"></script>
</head>


<body class="has-navbar-fixed-top"
    style="background-image: url(assets/Death_Grips_at_Brooklyn_Masonic_Temple\,_NYC_\(2015\).jpg); height:100vh">
    <div id=navbar>
        <script src="navbar.js"></script>
    </div>

    <section class="section container">
        <div class="box">
            <form class="form" method="post">
                <div class="control">
                    <div class="field">
                        <label for="username" class="label">Username : </label>
                        <input class="input" id="username" name="username" type="text" placeholder="Username"
                            autocomplete="username">
                    </div>
                    <div class="field">
                        <label for="password" class="label">Password : </label>
                        <input class="input" id="password" name="password" type="password" placeholder="Password"
                            autocomplete="password">
                    </div>
                    <div class="field">
                        <button class="button is-primary"><strong>Login</strong></button>
                    </div>
                    <?php
                    require_once "UserManager.php";

                    session_start();
                    $UserManager = new UserManager();


                    if ($_POST) {
                        $userArray = $UserManager->readAll();
                        $passwd = $_POST["password"];
                        $username = $_POST["username"];
                        $success = 0;

                        foreach ($userArray as $element) {
                            if ($username == $element[1] && $passwd == $element[2]) {
                                $success = 1;
                            }
                        }
                        if ($success == 1) {
                            header("Location: blog.php");
                            $_SESSION["username"] = $username;
                            die();
                        } else if ($success == 0) {
                            echo "EXTREMELY LOUD INCORRECT BUZZER";
                        }
                    }
                    ?>

                </div>
            </form>
        </div>
    </section>

</body>