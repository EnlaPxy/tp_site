<?php

session_start();

session_destroy();

$url = 'index.html';

function redirect($url, $statusCode = 303)
{
   header('Location: ' . $url, true, $statusCode);
   die();
}

redirect($url);