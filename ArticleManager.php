<?php

require_once "User.php";
class ArticleManager
{
    private $DB;
    public function __construct()
    {
        $DBname = "website";
        $username = "root";
        $password = "";
        $port = 3306;

        try {
            $this->setDB(new PDO("mysql:host=localhost;dbname=$DBname;port=$port;charset=utf8mb4", $username, $password));
        } catch (PDOException $error) {
            echo $error->getMessage();
        }

    }

    public function setDB($DB)
    {

        $this->DB = $DB;

    }

    public function create(Article $newArticle)
    {
        $request = $this->DB->prepare("INSERT INTO `articles`(title, content, author, creationDate) VALUES(:title, :content, :author, :creationDate)");
        $request->bindValue(":title", $newArticle->getTitle(), PDO::PARAM_STR);
        $request->bindValue(":content", $newArticle->getContent(), PDO::PARAM_STR);
        $request->bindValue(":author", $newArticle->getAuthor(), PDO::PARAM_STR);
        $request->bindValue(":creationDate", $newArticle->getCreationDate(), PDO::PARAM_STR);

        $request->execute();
    }

    // TODO: Maybe make it so there's a search function ? probably out of scope.
    // public function read($username)
    // {
    //     $request = $this->DB->prepare("SELECT * FROM `article` WHERE username = :username");
    //     $request->bindValue(":username", $username, PDO::PARAM_STR);
    //     $request->execute();
    //     $result = $request->fetch();
    //     return new User($result);
    // }

    public function readAll()
    {
        $request = $this->DB->prepare("SELECT * FROM `articles`");
        $request->execute();
        $result = $request->fetchall();
        $articlearray = [];
        if ($result[0] != NULL) {
            foreach ($result as $element) {
                array_push($articlearray, $element);
            }
        }
        return $articlearray;
    }

    public function delete($ID)
    {
        $request = $this->DB->prepare("DELETE * FROM `articles` WHERE ID = :id");
        $request->bindValue(":id", $ID, PDO::PARAM_INT);
        $request->execute();
    }

}

?>