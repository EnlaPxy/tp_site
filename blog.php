<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Bootleg</title>
  <link rel="stylesheet" href="bulma.css">
  <script type="text/javascript" src="navbar.js"></script>
</head>

<body class="has-navbar-fixed-top" style="background-color: rgb(44, 44, 44); height:100vh ;">
  <!--navbar code goes here-->
  <div id="navbar">
    <script src="navbar.js"></script>
  </div>
  <section class="container section">
  </section>

  <?php
  require_once "ArticleManager.php";

  $ArtMan = new ArticleManager();
  $artArray = $ArtMan->readAll();
  foreach ($artArray as $element) {
    $title = $element["title"];
    $author = $element["author"];
    $content = $element["content"];
    $date = $element["creationDate"];

    echo "
    <section class=\"container\">
    <br>
    <div class=\"box\">
        <article class=\"media\">
            <div class=\"media-content\">
                <div class=\"content\">
                    <p style=\"text-wrap: true\">
                      <strong>$title</strong>
                        <subtitle><small>$author</small> le <i>$date</i></subtitle>
                      <br>
                      <p class=\"content is-flex-wrap-wrap\">
                      $content
                      </p>
                    </p>
                </div>
            </div>
        </article>
    </div>
</section>";
  }
